package setyo.sharing.designpattern.structural.adapter.impl;

import setyo.sharing.designpattern.structural.adapter.interfaces.CarnivoreObject;
import setyo.sharing.designpattern.structural.adapter.interfaces.FlyingObject;
import setyo.sharing.designpattern.structural.adapter.model.Eagle;

public class EagleFlyingCarnivoreImpl implements FlyingObject, CarnivoreObject {
    private String name;
    private String color;

    public EagleFlyingCarnivoreImpl(Eagle eagle){
        this.name = eagle.getName();
        this.color = eagle.getColor();
    }

    @Override
    public String getFood() {
        return name+" berwarna : "+color+" memakan daging";
    }

    @Override
    public String getMoveBy() {
        return name+" berwarna : "+color+" bisa terbang";
    }
}
