package setyo.sharing.designpattern.structural.adapter.interfaces;

public interface CarnivoreObject {
    String getFood();
}
