package setyo.sharing.designpattern.structural.adapter;

import setyo.sharing.designpattern.structural.adapter.impl.BatMammalFlying;
import setyo.sharing.designpattern.structural.adapter.impl.CatMammalCarnivoreImpl;
import setyo.sharing.designpattern.structural.adapter.impl.EagleFlyingCarnivoreImpl;
import setyo.sharing.designpattern.structural.adapter.interfaces.CarnivoreObject;
import setyo.sharing.designpattern.structural.adapter.interfaces.FlyingObject;
import setyo.sharing.designpattern.structural.adapter.interfaces.MammalObject;
import setyo.sharing.designpattern.structural.adapter.model.Bat;
import setyo.sharing.designpattern.structural.adapter.model.Cat;
import setyo.sharing.designpattern.structural.adapter.model.Eagle;

import java.util.ArrayList;
import java.util.List;

public class Adapter {
    List<MammalObject> mammals = new ArrayList<>();
    List<FlyingObject> flyings = new ArrayList<>();
    List<CarnivoreObject> carnivore = new ArrayList<>();

    public Adapter(){
        mammals.add(new BatMammalFlying(new Bat("kalelawar1", "hitam")));
        flyings.add(new BatMammalFlying(new Bat("kalelawar2", "abu-abu")));

        mammals.add(new CatMammalCarnivoreImpl(new Cat("Kucing1", "orange")));
        carnivore.add(new CatMammalCarnivoreImpl(new Cat("Kucing2", "hitam")));

        carnivore.add(new EagleFlyingCarnivoreImpl(new Eagle("Elang1", "coklat")));
        flyings.add(new EagleFlyingCarnivoreImpl(new Eagle("Elang2", "hitam")));
    }

    public void hewanTerbang(){
        flyings.forEach(hewan -> {
            System.out.print(hewan.getMoveBy());
        });
    }

    public void hewanPemakanDaging(){
        carnivore.forEach(hewan -> {
            System.out.print(hewan.getFood());
        });
    }

    public void hewanMelahirkan(){
        mammals.forEach(hewan -> {
            System.out.print(hewan.getBirthType());
        });
    }
}
