package setyo.sharing.designpattern.structural.adapter.impl;

import setyo.sharing.designpattern.structural.adapter.interfaces.FlyingObject;
import setyo.sharing.designpattern.structural.adapter.interfaces.MammalObject;
import setyo.sharing.designpattern.structural.adapter.model.Bat;

public class BatMammalFlying implements MammalObject, FlyingObject {
    private String name;
    private String color;

    public BatMammalFlying(Bat bat){
        this.name = bat.getName();
        this.color = bat.getColor();
    }

    @Override
    public String getMoveBy() {
        return name+" berwarna : "+color+" bisa terbang";
    }

    @Override
    public String getBirthType() {
        return name+" berwarna : "+color+" melahirkan anaknya";
    }
}
