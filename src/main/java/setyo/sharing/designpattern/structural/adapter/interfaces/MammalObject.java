package setyo.sharing.designpattern.structural.adapter.interfaces;

public interface MammalObject {
    String getBirthType();
}
