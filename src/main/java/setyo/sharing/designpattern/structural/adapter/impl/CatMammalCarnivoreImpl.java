package setyo.sharing.designpattern.structural.adapter.impl;

import setyo.sharing.designpattern.structural.adapter.interfaces.CarnivoreObject;
import setyo.sharing.designpattern.structural.adapter.interfaces.MammalObject;
import setyo.sharing.designpattern.structural.adapter.model.Cat;

public class CatMammalCarnivoreImpl implements MammalObject, CarnivoreObject {
    private String name;
    private String color;

    public CatMammalCarnivoreImpl(Cat cat){
        this.name = cat.getName();
        this.color = cat.getColor();
    }

    @Override
    public String getFood() {
        return name+" berwarna : "+color+" adalah hewan pemakan daging";
    }

    @Override
    public String getBirthType() {
        return name+" berwarna : "+color+" melahirkan anaknya";
    }
}
