package setyo.sharing.designpattern.structural.adapter.interfaces;

public interface FlyingObject {
    String getMoveBy();
}
