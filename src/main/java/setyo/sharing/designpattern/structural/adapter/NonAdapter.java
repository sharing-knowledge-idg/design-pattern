package setyo.sharing.designpattern.structural.adapter;

import setyo.sharing.designpattern.structural.adapter.model.Bat;
import setyo.sharing.designpattern.structural.adapter.model.Cat;
import setyo.sharing.designpattern.structural.adapter.model.Eagle;

import java.util.ArrayList;
import java.util.List;

public class NonAdapter {
    List<Object> animal = new ArrayList<>();

    public NonAdapter(){
        animal.add(new Bat("kalelawar1", "hitam"));
        animal.add(new Bat("kalelawar2", "abu-abu"));

        animal.add(new Cat("Kucing1", "orange"));
        animal.add(new Cat("Kucing2", "hitam"));

        animal.add(new Eagle("Elang1", "coklat"));
        animal.add(new Eagle("Elang2", "hitam"));
    }

    public void hewanTerbang(){
        for (Object hewan: animal) {
            if(hewan instanceof Cat) {
                Bat bat = (Bat) hewan;
                System.out.print("Hewan terbang ini berjenis kalelawar dengan nama :"+bat.getName()+" berwarna : "+bat.getColor());
            }
            if(hewan instanceof Eagle){
                Eagle eagle = (Eagle) hewan;
                System.out.print("Hewan terbang ini berjenis elang dengan nama :"+eagle.getName()+" berwarna : "+eagle.getColor());
            }
        }
    }

    public void hewanPemakanDaging(){
        for (Object hewan: animal) {
            if(hewan instanceof Cat) {
                Cat cat = (Cat) hewan;
                System.out.print("Hewan pemakan daging ini berjenis kucing dengan nama :"+cat.getName()+" berwarna : "+cat.getColor());
            }
            if(hewan instanceof Eagle){
                Eagle eagle = (Eagle) hewan;
                System.out.print("Hewan pemakan daging ini berjenis elang dengan nama :"+eagle.getName()+" berwarna : "+eagle.getColor());
            }
        }
    }

    public void hewanMelahirkan(){
        for (Object hewan: animal) {
            if(hewan instanceof Cat) {
                Bat bat = (Bat) hewan;
                System.out.print("Hewan melahirkan ini berjenis kalelawar dengan nama :"+bat.getName()+" berwarna : "+bat.getColor());
            }
            if(hewan instanceof Eagle){
                Cat cat = (Cat) hewan;
                System.out.print("Hewan melahirkan ini berjenis kucing dengan nama :"+cat.getName()+" berwarna : "+cat.getColor());
            }
        }
    }
}
