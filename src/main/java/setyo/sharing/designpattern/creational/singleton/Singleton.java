package setyo.sharing.designpattern.creational.singleton;

public class Singleton {

    public void printArticle1(){
        String htmlBody = ConfigApps.getRestTemplate().getForObject("http://web.article1.com", String.class);

        System.out.print(htmlBody);
    }

    public void printArticle2(){
        String htmlBody = ConfigApps.getRestTemplate().getForObject("http://web.article2.com", String.class);

        System.out.print(htmlBody);
    }
}
