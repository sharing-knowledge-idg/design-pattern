package setyo.sharing.designpattern.creational.singleton;

import org.springframework.web.client.RestTemplate;

public class ConfigApps {
    private static RestTemplate restTemplate;

    public static RestTemplate getRestTemplate(){
        return restTemplate == null ? restTemplate=new RestTemplate() : restTemplate;
    }
}
