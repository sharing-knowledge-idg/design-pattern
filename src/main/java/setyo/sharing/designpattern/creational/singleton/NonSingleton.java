package setyo.sharing.designpattern.creational.singleton;

import org.springframework.web.client.RestTemplate;

public class NonSingleton {

    public void printArticle1(){
        String articles = new RestTemplate().getForObject("http://web.article1.com", String.class);

        System.out.print(articles);
    }

    public void printArticle2(){
        String articles = new RestTemplate().getForObject("http://web.article2.com", String.class);

        System.out.print(articles);
    }
}
